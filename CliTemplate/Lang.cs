﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate {
  public class Lang {
    public static readonly string CommandToManyArgs = "<b>Invalid argument \'<p>{0}<b>\'. <i>Use the command like this:";
  }
}
