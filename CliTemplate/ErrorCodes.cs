﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate {
  public enum ErrorCodes {
    SUCCESS =  0,
    INVALID_COMMAND = 1,
    INVALID_PARAMS = 2,
    EXCEPTION = 3,
  }
}
