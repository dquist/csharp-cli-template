﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CliTemplate.Cmd;

namespace CliTemplate.Core {

  /// <summary>
  /// The core app implemenation
  /// </summary>
  public class CoreApp : IApp {

    private readonly IShell m_Shell;
    private readonly ICommandHandler m_CommandHandler;


    /// <summary>
    /// Creates a new CoreApp instance
    /// </summary>
    public CoreApp() {
      m_Shell = new FormattedConsole(new ConsoleShell());
      m_CommandHandler = new CoreCommandHandler(this);

    }

    public string CommandRoot {
      get { return "banner"; }
    }


    /// <summary>
    /// Runs the app with arguments
    /// </summary>
    /// <param name="args">The command line arguments</param>
    /// <returns>The return code</returns>
    public ErrorCodes Run(string[] args) {
      ErrorCodes code = m_CommandHandler.HandleCommand(m_Shell, new List<string>(args));

#if DEBUG
      m_Shell.WriteLine("Press any key to continue . . .");
      while (!Console.KeyAvailable) { }
#endif
      return code;
    }
  }
}
