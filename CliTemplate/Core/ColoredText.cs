﻿
namespace CliTemplate.Core {
  class ColoredText {
    private readonly ShellColor m_Color;
    private readonly string m_Text;

    public ColoredText(ShellColor color, string text) {
      m_Color = color;
      m_Text = text;
    }

    public ShellColor Color {
      get { return m_Color; }
    }

    public string Text {
      get { return m_Text; }
    }
  }
}
