﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CliTemplate.Core {
  class FormattedConsole : IShell {

    // Matches color codes of the format '&X' inside a string, where X is any number 0-9
    // or a letter A-G. You can escape this sequence with a double &&.
    private static readonly Regex m_ColorMatch = new Regex(@"(?<!&)&(?i:[\d|A-G])");

    // Matches the ampersand codes to console colors
    private static readonly Dictionary<char, ShellColor> m_ColorMap = new Dictionary<char, ShellColor>() {
      { '0', ShellColor.Black },
      { '1', ShellColor.Navy },
      { '2', ShellColor.Green },
      { '3', ShellColor.Teal },
      { '4', ShellColor.Red },
      { '5', ShellColor.Purple },
      { '6', ShellColor.Gold },
      { '7', ShellColor.Silver },
      { '8', ShellColor.Gray },
      { '9', ShellColor.Blue },
      { 'A', ShellColor.Lime },
      { 'B', ShellColor.Aqua },
      { 'C', ShellColor.Rose },
      { 'D', ShellColor.Pink },
      { 'E', ShellColor.Yellow },
      { 'F', ShellColor.White },
      { 'G', ShellColor.Reset },
    };

    private readonly IShell m_Shell;
    private readonly Object m_Lock = new Object();


    /// <summary>
    /// Creates a new FormattedShell instance
    /// </summary>
    /// <param name="shell">The underlying shell implementation</param>
    public FormattedConsole(IShell shell) {
      m_Shell = shell;
    }


    public void Write(string value) {
      WriteFormatEntries(ParseColor(value));
    }

    public void Write(string format, params object[] arg) {
      Write(String.Format(format, arg));
    }

    public void WriteLine(string value) {
      WriteFormatEntries(ParseColor(value), true);
    }

    public void WriteLine(string format, params object[] arg) {
      WriteLine(String.Format(format, arg));
    }


    /// <summary>
    /// Gets or sets the current text color
    /// </summary>
    public ShellColor Color {
      get { return m_Shell.Color; }
      set {
        lock(m_Lock) {
          m_Shell.Color = value;
        }
      }
    }


    /// <summary>
    /// Parses the color tags in the string
    /// </summary>
    /// <param name="value">The string value</param>
    /// <returns>The parsed string</returns>
    private string ParseTags(string value) {
      value = value
        .Replace("<g>", "<good>")
        .Replace("<i>", "<info>")
        .Replace("<b>", "<bad>")
        .Replace("<c>", "<aqua>")
        .Replace("<p>", "<teal>")
        .Replace("<r>", "<reset>");

      value = value
        .Replace("<good>", "<green>")
        .Replace("<info>", "<gold>")
        .Replace("<bad>", "<rose>");

      return value
        .Replace("<black>", "&0")
        .Replace("<navy>", "&1")
        .Replace("<green>", "&2")
        .Replace("<teal>", "&3")
        .Replace("<red>", "&4")
        .Replace("<purple>", "&5")
        .Replace("<gold>", "&6")
        .Replace("<silver>", "&7")
        .Replace("<gray>", "&8")
        .Replace("<blue>", "&9")
        .Replace("<lime>", "&A")
        .Replace("<aqua>", "&B")
        .Replace("<rose>", "&C")
        .Replace("<pink>", "&D")
        .Replace("<yellow>", "&E")
        .Replace("<white>", "&F")
        .Replace("<reset>", "&G");
    }

    /// <summary>
    /// Parses an ampersand color formatted string into a list of ColorText entries
    /// </summary>
    /// <param name="value">The value to parse</param>
    /// <returns>The list of colored text entries</returns>
    private List<ColoredText> ParseColor(string value) {
      value = ParseTags(value);

      MatchCollection matches = m_ColorMatch.Matches(value);
      List<ColoredText> colorEntries = new List<ColoredText>();
      int index = 0;
      ShellColor color = ShellColor.Reset;

      foreach (Match m in matches) {
        if (m.Index > 0) {
          string text = value.Substring(index, m.Index - index);
          index = m.Index;
          colorEntries.Add(new ColoredText(color, text));
        }

        char key = m.Value.ToUpper()[1];
        color = m_ColorMap[key];
        index += 2;
      }

      if (index < value.Length) {
        string text = value.Substring(index, value.Length - index);
        colorEntries.Add(new ColoredText(color, text));
      }

      return colorEntries;
    }

    /// <summary>
    /// Writes a list of formatted entries to the console
    /// </summary>
    /// <param name="entries">The entries to write</param>
    /// <param name="newLine">Whether to add a new line or not</param>
    private void WriteFormatEntries(List<ColoredText> entries, bool newLine = false) {
      lock(m_Lock) {
        foreach (ColoredText e in entries) {
          m_Shell.Color = e.Color;
          m_Shell.Write(e.Text);
        }

        if (newLine) {
          m_Shell.Write("\n");
        }
      }
    }
  }
}
