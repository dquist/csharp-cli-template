﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CliTemplate.Cmd;
using CliTemplate.Exceptions;

namespace CliTemplate.Core {
  internal class CoreCommandHandler : ICommandHandler {

    private readonly IApp m_App;
    private readonly HashSet<ICommand> m_Commands;

    public CoreCommandHandler(IApp app) {
      m_App = app;
      m_Commands = new HashSet<ICommand>();

      m_Commands.Add(new CmdTemplate(app));
      m_Commands.Add(new CmdAutoHelp(app));
    }


    /// <summary>
    /// Handles a given command
    /// </summary>
    /// <param name="shell">The sender shell</param>
    /// <param name="args">The command arguments</param>
    /// <returns>The return code</returns>
    public ErrorCodes HandleCommand(IShell shell, List<string> args) {
      try {
        var command = m_Commands.FirstOrDefault(c => c.Aliases.Contains(args[0]));
        if (command != null) {
          args.RemoveAt(0);
          command.Execute(shell, args);
          return ErrorCodes.SUCCESS;
        } else {
          throw new InvalidCommandException(args[0]);
        }

      } catch(Exception ex) {
        return HandleException(shell, ex);
      }
    }

    private ErrorCodes HandleException(IShell shell, Exception ex) {
      var code = ErrorCodes.EXCEPTION;

      if (ex is InvalidCommandException) {
        code = ErrorCodes.INVALID_COMMAND;
      }
      else if (ex is InvalidArgumentException) {
        code = ErrorCodes.INVALID_PARAMS;

        shell.WriteLine(string.Format("<b>Error[{0}]: <r>{1}", (int)code, ex.Message));
        shell.WriteLine("\nUse the command like this: ");
        shell.WriteLine(string.Format("  <c>{0}<r> {1}", m_App.CommandRoot, (ex as InvalidArgumentException).UsageTemplate));
      } else {
        shell.WriteLine(string.Format("<b>Internal error[{0}]: <r>[{1}] {2}", (int)code, ex.GetType(), ex.Message));
        shell.WriteLine(ex.StackTrace);
      }

      shell.WriteLine("");

      return code;
    }
  }
}
