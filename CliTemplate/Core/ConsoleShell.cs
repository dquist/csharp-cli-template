﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Core {
  internal class ConsoleShell : IShell {

    private ShellColor m_Color = ShellColor.White;

    public ConsoleShell() {
      Console.ResetColor();
    }

    public void Write(string value) {
      Console.Write(value);
    }

    public void Write(string format, params object[] arg) {
      Console.Write(format, arg);
    }

    public void WriteLine(string value) {
      Console.WriteLine(value);
    }

    public void WriteLine(string format, params object[] arg) {
      Console.WriteLine(format, arg);
    }

    public ShellColor Color {
      get { return m_Color; }
      set {
        m_Color = value;
        if ((int)m_Color < 16) {
          Console.ForegroundColor = (ConsoleColor)value;
        } else {
          switch(value) {
            case ShellColor.Reset:
              Console.ResetColor();
              break;

            default:
              break;
          }
        }
      }
    }
  }
}
