﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {
  public interface ICommandHandler {

    /// <summary>
    /// Handles a given command
    /// </summary>
    /// <param name="shell">The sender shell</param>
    /// <param name="args">The command arguments</param>
    /// <returns>The return code</returns>
    ErrorCodes HandleCommand(IShell shell, List<string> args);
  }
}
