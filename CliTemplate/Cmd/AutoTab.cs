﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {

  /// <summary>
  /// Class to assist in command-line tab completion
  /// </summary>
  internal class AutoTab : IAutoTab {

    private readonly string m_Name;
    private readonly string m_HelpString;


    /// <summary>
    /// Creates a new AutoTab instance
    /// </summary>
    /// <param name="name">The auto-tab name</param>
    /// <param name="helpString">The help string</param>
    public AutoTab(string name, string helpString) {
      m_Name = name;
      m_HelpString = helpString;
    }


    /// <summary>
    /// Gets the auto-tab name
    /// </summary>
    public string Name {
      get {
        return m_Name;
      }
    }


    /// <summary>
    /// Gets the auto-tab help string
    /// </summary>
    public string HelpString {
      get {
        return m_HelpString;
      }
    }
  }
}
