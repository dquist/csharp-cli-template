﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {

  /// <summary>
  /// An optional command argument
  /// </summary>
  internal class OptionalCommandArg : ICommandArg {

    private readonly string m_Name;
	  private readonly string m_DefaultValue;
	  private readonly IAutoTab m_AutoTab;


    /// <summary>
    /// Creates a new OptionalCommandArg instance
    /// </summary>
    /// <param name="name">The argument name</param>
    /// <param name="defaultValue">The default value</param>
    /// <param name="autoTab">The auto tab instance</param>
    public OptionalCommandArg(string name, string defaultValue, IAutoTab autoTab) {
      m_Name = name;
      m_DefaultValue = defaultValue;
      m_AutoTab = autoTab;
    }


    /// <summary>
    /// Creates a new OptionalCommandArg instance
    /// </summary>
    /// <param name="name">The argument name</param>
    /// <param name="autoTab">The auto tab instance</param>
    public OptionalCommandArg(string name, IAutoTab autoTab) :
      this(name, null, autoTab) { }


    /// <summary>
    /// Creates a new OptionalCommandArg instance
    /// </summary>
    /// <param name="name">The argument name</param>
    /// <param name="defaultValue">The default value</param>
    public OptionalCommandArg(string name, string defaultValue) :
      this(name, defaultValue, null) { }


    /// <summary>
    /// Creates a new OptionalCommandArg instance
    /// </summary>
    /// <param name="name">The argument name</param>
    public OptionalCommandArg(string name) :
      this(name, null, null) { }


    /// <summary>
    /// Gets the argument name
    /// </summary>
    public string Name {
      get {
        return m_Name;
      }
    }


    /// <summary>
    /// Gets whether the argument is required
    /// </summary>
    public bool IsRequired {
      get {
        return false;
      }
    }

    /// <summary>
    /// Gets whether this is an auto-tab argument
    /// </summary>
    public bool IsAutoTab {
      get {
        return m_AutoTab != null;
      }
    }

    /// <summary>
    /// Gets the auto-tab instance
    /// </summary>
    public IAutoTab AutoTab {
      get {
        return m_AutoTab;
      }
    }

    /// <summary>
    /// Converts the argument to a string
    /// </summary>
    /// <returns>The string representation</returns>
    public override string ToString() {
      if (m_DefaultValue != null) {
        return "[" + m_Name + "=" + m_DefaultValue + "]";
      }
      return "[" + m_Name + "]";
    }
  }
}
