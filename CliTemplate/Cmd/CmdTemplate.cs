﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {
  class CmdTemplate : BaseCommand {

    public CmdTemplate(IApp app) :
      base(app) {
      AddAlias("build");
      AddArgument(Required("project_path"));
      ErrorOnToManyArgs = false;

      HelpShort = "This is a template command";

    }

    protected override void Perform() {
      WriteLine("<g>The template command completed.");
    }
  }
}
