﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {

  /// <summary>
  /// A required command argument
  /// </summary>
  internal class RequiredCommandArg : ICommandArg {

    private readonly string m_Name;
	  private readonly IAutoTab m_AutoTab;


    /// <summary>
    /// Creates a new RequiredCommandArg instance
    /// </summary>
    /// <param name="name">The argument name</param>
    /// <param name="autoTab">The auto tab instance</param>
    public RequiredCommandArg(string name, IAutoTab autoTab) {
      m_Name = name;
      m_AutoTab = autoTab;
    }


    /// <summary>
    /// Creates a new RequiredCommandArg instance
    /// </summary>
    /// <param name="name">The argument name</param>
    public RequiredCommandArg(string name) :
      this(name, null) { }


    /// <summary>
    /// Gets the argument name
    /// </summary>
    public string Name {
      get {
        return m_Name;
      }
    }


    /// <summary>
    /// Gets whether the argument is required
    /// </summary>
    public bool IsRequired {
      get {
        return true;
      }
    }

    /// <summary>
    /// Gets whether this is an auto-tab argument
    /// </summary>
    public bool IsAutoTab {
      get {
        return m_AutoTab != null;
      }
    }

    /// <summary>
    /// Gets the auto-tab instance
    /// </summary>
    public IAutoTab AutoTab {
      get {
        return m_AutoTab;
      }
    }

    /// <summary>
    /// Converts the argument to a string
    /// </summary>
    /// <returns>The string representation</returns>
    public override string ToString() {
      return "<" + m_Name + ">";
    }
  }
}
