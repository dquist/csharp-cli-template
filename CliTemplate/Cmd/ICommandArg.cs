﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {

  /// <summary>
  /// A command argument that is sent to a Command
  /// </summary>
  public interface ICommandArg {

    /// <summary>
    /// Gets the argument name
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Whether the argument is required
    /// </summary>
    bool IsRequired { get; }

    /// <summary>
    /// Whether this is an auto-tab argument
    /// </summary>
    bool IsAutoTab { get; }

    /// <summary>
    /// Gets the <see cref="AutoTab"/> instance if it exists
    /// </summary>
    IAutoTab AutoTab { get; }
  }
}
