﻿using CliTemplate.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {
  internal abstract class BaseCommand : ICommand {

    private readonly IApp m_App;
    private List<ICommand> m_SubCommands = new List<ICommand>();
    private List<ICommand> m_CommandChain = new List<ICommand>();
    private readonly List<string> m_Aliases = new List<string>();
    private List<ICommandArg> m_CommandArgs = new List<ICommandArg>();
    private List<string> m_Args;

    private string m_HelpShort = "";
    private List<string> m_HelpLong = new List<string>();
    private bool m_ErrorOnToManyArgs = true;
    private IShell m_Shell;


    /// <summary>
    /// Creates a new BaseCommand instance
    /// </summary>
    /// <param name="app">The app interface</param>
    protected BaseCommand(IApp app) {
      m_App = app;
    }


    /// <summary>
    /// Performs the command
    /// </summary>
    protected abstract void Perform();


    /// <summary>
    /// Performs the command tab list completion
    /// </summary>
    /// <returns>The array of tab items</returns>
    protected virtual List<string> PerformTabList() {
      return null;
    }


    /// <summary>
    /// Executes the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command arguments</param>
    /// <param name="commandChain">The command chain</param>
    public void Execute(IShell shell, List<string> args, List<ICommand> commandChain) {
      if (PrepareCommand(shell, args, commandChain)) {
        Perform();
      }
    }


    /// <summary>
    /// Executes the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    /// <returns>The return code</returns>
    public void Execute(IShell shell, List<string> args) {
      Execute(shell, args, new List<ICommand>());
    }


    /// <summary>
    /// GGets the tab list for the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    /// <param name="commandChain">The command chain</param>
    /// <returns></returns>
    public List<string> GetTabList(IShell shell, List<string> args, List<ICommand> commandChain) {
      return PrepareTab(shell, args, commandChain);
    }


    /// <summary>
    /// Gets the tab list for the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    /// <returns></returns>
    public List<string> GetTabList(IShell shell, List<string> args) {
      return GetTabList(shell, args, new List<ICommand>());
    }


    /// <summary>
    /// Prepares the command for execution
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    /// <param name="commandChain">The command chain</param>
    /// <returns>true if the command should execute</returns>
    private bool PrepareCommand(IShell shell, List<string> args, List<ICommand> commandChain) {
      m_Shell = shell;
      m_Args = args;
      m_CommandChain = commandChain;
      throw new Exception("testing");

      // Try to find a matching sub-command
      if (args.Count > 0) {
        foreach (ICommand subCommand in m_SubCommands) {
          if (subCommand.Aliases.Contains(args[0])) {
            args.RemoveAt(0);
            commandChain.Add(this);
            subCommand.Execute(shell, args, commandChain);
            return false;
          }
        }
      }

      CheckValidArgs(args);
      return true;
    }


    /// <summary>
    /// Prepares a tab command for execution
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    /// <param name="commandChain">The command chain</param>
    /// <returns></returns>
    private List<string> PrepareTab(IShell shell, List<string> args, List<ICommand> commandChain) {
      m_Shell = shell;
      m_Args = args;
      m_CommandChain = commandChain;

      // Find a matching sub-command
      if (args.Count > 0) {
        foreach (ICommand subCommand in m_SubCommands) {
          if (subCommand.Aliases.Contains(args[0])) {
            args.RemoveAt(0);
            commandChain.Add(this);
            return subCommand.GetTabList(shell, args, commandChain);
          }
        }
      }

      List<string> subCommands = new List<string>();

      // Get the tab list of any matching sub-commands
      if (args.Count > 0) {
        foreach (ICommand subCommand in m_SubCommands) {
          foreach (string alias in subCommand.Aliases) {
            if (alias.StartsWith(args[0])) {
              subCommands.Add(alias);
              break;
            }
          }
        }
      }

      // Return the matching tab commands if there are any
      if (m_SubCommands.Count > 0) {
        if (subCommands.Count > 0) {
          return subCommands;
        } else {
          return null;
        }
      }

      // This is the case when someone tabs at the end of a command before pressing space
      if (args.Count == 0 || (args.Count == 1 && args[0] == "")) {
        return null;
      }

      // Check if the arg is an auto-tab type
      if (args.Count > m_CommandArgs.Count) {
        WriteLine("<i>No more arguments are needed.");
        return null;
      }

      ICommandArg tabArg = m_CommandArgs[args.Count - 1];
      if (tabArg == null) {
        return null;
      }

      List<string> tabList = new List<string>();

      if (tabArg.IsAutoTab) {
        tabList = GetAutoTab(tabArg.AutoTab.Name, args[args.Count - 1]);
        if (tabList != null) {
          return tabList;
        }
      }

      // Otherwise perform the tab routine for the command itself
      tabList = PerformTabList();
      if (tabList != null) {
        return tabList;
      }

      // If still nothing found, send the help message
      if (tabArg.IsAutoTab && tabArg.AutoTab.HelpString != null) {
        WriteLine("<i>" + tabArg.AutoTab.HelpString);
      }
      return null;
    }


    /// <summary>
    /// Adds a sub-command
    /// </summary>
    /// <param name="subCommand">The sub-command to add</param>
    public void AddSubCommand(ICommand subCommand) {
      //subCommand.CommandChain.AddRange(m_CommandChain);
      //subCommand.CommandChain.Add(this);
      m_SubCommands.Add(subCommand);
    }


    /// <summary>
    /// Gets the command's subcommands
    /// </summary>
    public IList<ICommand> SubCommands {
      get { return new ReadOnlyCollection<ICommand>(m_SubCommands); }
    }


    /// <summary>
    /// Gets or sets the short help text
    /// </summary>
    public string HelpShort {
      get { return m_HelpShort; }
      protected set { m_HelpShort = value; }
    }


    /// <summary>
    /// Gets or sets the long help text
    /// </summary>
    public IList<string> HelpLong {
      get { return m_HelpLong; }
      protected set {
        m_HelpLong.Clear();
        m_HelpLong.AddRange(value);
      }
    }


    /// <summary>
    /// Gets the command aliases
    /// </summary>
    public IList<string> Aliases {
      get { return new ReadOnlyCollection<string>(m_Aliases); }
    }


    /// <summary>
    /// Adds a command alias
    /// </summary>
    /// <param name="alias">The alias to add</param>
    protected void AddAlias(string alias) {
      m_Aliases.Add(alias);
    }


    /// <summary>
    /// Gets the command chain
    /// </summary>
    protected IList<ICommand> CommandChain {
      get { return new ReadOnlyCollection<ICommand>(m_CommandChain); }
    }


    /// <summary>
    /// Adds a command argument
    /// </summary>
    /// <param name="arg">The argument to add</param>
    protected void AddArgument(ICommandArg arg) {
      m_CommandArgs.Add(arg);
    }


    /// <summary>
    /// Gets the number of required args
    /// </summary>
    protected int NumRequiredArgs {
      get { return m_CommandArgs.Count(a => a.IsRequired); }
    }


    /// <summary>
    /// Gets or sets whether the command errors on too many arguments
    /// </summary>
    protected bool ErrorOnToManyArgs {
      get { return m_ErrorOnToManyArgs; }
      set { m_ErrorOnToManyArgs = value; }
    }


    /// <summary>
    /// Checks whether the correct number of arguments have been supplied
    /// </summary>
    /// <param name="args">The arguments</param>
    protected void CheckValidArgs(List<string> args) {
      if (args.Count < NumRequiredArgs) {
        throw new Exceptions.InvalidArgumentException("<b>Too few arguments.", GetUsageTemplate());
      }

      if (args.Count > m_CommandArgs.Count && m_ErrorOnToManyArgs) {
        List<string> extraArgs = m_Args.GetRange(m_CommandArgs.Count, m_Args.Count - m_CommandArgs.Count);
        string format = string.Format("Invalid argument{0}: <r>'{1}'",
          extraArgs.Count > 1 ? "s" : "",
          TextUtil.Implode(extraArgs, " "));

        throw new Exceptions.InvalidArgumentException(format, GetUsageTemplate());
      }
    }


    /// <summary>
    /// Gets the usage template for the command
    /// </summary>
    /// <param name="commandChain">The command chain</param>
    /// <param name="addShortHelp">Whether to add a short help</param>
    /// <returns>The usage template</returns>
    public string GetUsageTemplate(IList<ICommand> commandChain, bool addShortHelp) {
      StringBuilder ret = new StringBuilder();
      ret.Append("<c>");

      foreach (ICommand c in commandChain) {
        ret.Append(TextUtil.Implode(c.Aliases, ","));
        ret.Append(' ');
      }

      ret.Append(TextUtil.Implode(m_Aliases, ","));

      if (m_CommandArgs.Count > 0) {
        foreach (ICommandArg a in m_CommandArgs) {
          ret.Append("<p> ");
          ret.Append(a.ToString());
        }
      }

      if (addShortHelp) {
        ret.Append(" <i>");
        ret.Append(HelpShort);
      }

      return ret.ToString();
    }


    /// <summary>
    /// Gets the usage template for the command
    /// </summary>
    /// <param name="addShortHelp">Whether to append the short help text</param>
    /// <returns>The usage template</returns>
    public string GetUsageTemplate(bool addShortHelp) {
      return GetUsageTemplate(m_CommandChain, addShortHelp);
    }


    /// <summary>
    /// Gets the usage template for the command
    /// </summary>
    /// <returns>The usage template</returns>
    public string GetUsageTemplate() {
      return GetUsageTemplate(false);
    }


    /// <summary>
    /// Gets a custom auto-tab list.
    /// 
    /// This should be overridden by commands that wish to provide their own
    /// auto-tab implementation.
    /// </summary>
    /// <param name="tabName">The auto-tab name</param>
    /// <param name="pattern">The pattern</param>
    /// <returns>The tab list if any results, otherwise null</returns>
    protected virtual List<string> GetCustomAutoTab(string tabName, string pattern) {
      return null;
    }


    /// <summary>
    /// Gets the tab values from the available auto-tab implementations
    /// </summary>
    /// <param name="name">The auto-tab name</param>
    /// <param name="pattern">The pattern</param>
    /// <returns>The tab list if any results, otherwise null</returns>
    private List<string> GetAutoTab(string name, string pattern) {
      List<string> tabList = new List<string>();

      switch (name) {
        default:
          List<string> customTab = GetCustomAutoTab(name, pattern);
          if (customTab != null) {
            tabList.AddRange(customTab);
          }
          break;
      }

      if (tabList.Count == 0) {
        return null;
      }

      return tabList;
    }


    /// <summary>
    /// Sends a message to the shell
    /// </summary>
    /// <param name="message">The message to send</param>
    protected void WriteLine(string message) {
      m_Shell.WriteLine(message);
    }


    /// <summary>
    /// Sends a message to the shell
    /// </summary>
    /// <param name="format">The string format</param>
    /// <param name="args">The format args</param>
    protected void WriteLine(string format, params object[] args) {
      m_Shell.WriteLine(string.Format(format, args));
    }


    protected static IAutoTab Auto(string name, string help) {
      return new AutoTab(name, help);
    }

    protected static IAutoTab Auto(string name) {
      return new AutoTab(name, null);
    }

    protected static ICommandArg Required(string name, IAutoTab autoTab) {
      return new RequiredCommandArg(name, autoTab);
    }

    protected static ICommandArg Required(string name) {
      return Required(name, null);
    }

    protected static ICommandArg Optional(string name, string defaultValue, IAutoTab autoTab) {
      return new OptionalCommandArg(name, defaultValue, autoTab);
    }

    protected static ICommandArg Optional(string name, string defaultValue) {
      return Optional(name, defaultValue, null);
    }

    protected static ICommandArg Optional(string name, IAutoTab autoTab) {
      return Optional(name, null, autoTab);
    }

    protected static ICommandArg Optional(string name) {
      return Optional(name, null, null);
    }
  }
}
