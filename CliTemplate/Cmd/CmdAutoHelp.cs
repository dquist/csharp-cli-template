﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {
  class CmdAutoHelp : BaseCommand {

    public CmdAutoHelp(IApp app) :
      base(app) {
      AddAlias("help");
    }

    protected override void Perform() {
      if (CommandChain.Count == 0) {
        return;
      }

      ICommand cmd = CommandChain[CommandChain.Count - 1];
      List<string> lines = new List<string>();
      lines.AddRange(cmd.HelpLong);

      foreach (ICommand c in cmd.SubCommands) {
        lines.Add(c.GetUsageTemplate(CommandChain, true));
      }
    }
  }
}
