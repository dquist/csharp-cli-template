﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {

  /// <summary>
  /// A command argument that can have the tab list automatically generated
  /// </summary>
  public interface IAutoTab {

    /// <summary>
    /// Gets the auto-tab name
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Gets the auto-tab help string
    /// </summary>
    string HelpString { get; }
  }
}
