﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Cmd {

  /// <summary>
  /// An application command that can be executed
  /// </summary>
  public interface ICommand {

    /// <summary>
    /// Executes the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command arguments</param>
    /// <param name="commandChain">The command chain</param>
    void Execute(IShell shell, List<string> args, List<ICommand> commandChain);


    /// <summary>
    /// Executes the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    void Execute(IShell shell, List<string> args);


    /// <summary>
    /// GGets the tab list for the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    /// <param name="commandChain">The command chain</param>
    /// <returns></returns>
    List<string> GetTabList(IShell shell, List<string> args, List<ICommand> commandChain);


    /// <summary>
    /// Gets the tab list for the command
    /// </summary>
    /// <param name="shell">The command shell</param>
    /// <param name="args">The command args</param>
    /// <returns></returns>
    List<string> GetTabList(IShell shell, List<string> args);


    /// <summary>
    /// Gets the usage template for the command
    /// </summary>
    /// <param name="commandChain">The command chain</param>
    /// <param name="addShortHelp">Whether to add a short help</param>
    /// <returns>The usage template</returns>
    string GetUsageTemplate(IList<ICommand> commandChain, bool addShortHelp);


    /// <summary>
    /// Gets the usage template for the command
    /// </summary>
    /// <param name="addShortHelp">Whether to append the short help text</param>
    /// <returns>The usage template</returns>
    string GetUsageTemplate(bool addShortHelp);


    /// <summary>
    /// Gets the usage template for the command
    /// </summary>
    /// <returns>The usage template</returns>
    string GetUsageTemplate();


    /// <summary>
    /// Gets the command aliases
    /// </summary>
    IList<string> Aliases { get; }


    /// <summary>
    /// Gets or sets the short help text
    /// </summary>
    string HelpShort { get; }


    /// <summary>
    /// Gets or sets the long help text
    /// </summary>
    IList<string> HelpLong { get; }


    /// <summary>
    /// Gets the command's subcommands
    /// </summary>
    IList<ICommand> SubCommands { get; }
  }
}
