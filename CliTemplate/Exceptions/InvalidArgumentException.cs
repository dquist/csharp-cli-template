﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Exceptions {
  public class InvalidArgumentException : Exception {

    private readonly string m_UsageTemplate;

    public InvalidArgumentException(string message, string usageTemplate)
        : base(message)
    {
      m_UsageTemplate = usageTemplate;
    }

    public string UsageTemplate {
      get { return m_UsageTemplate; }
    }
  }
}
