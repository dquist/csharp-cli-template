﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Exceptions {
  public class InvalidCommandException : Exception {

    public InvalidCommandException(string command) :
      base(string.Format("Unknown command {0}.", command)) {
    }
  }
}
