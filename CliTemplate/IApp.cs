﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate {
  public interface IApp {

    /// <summary>
    /// Gets the root command
    /// </summary>
    string CommandRoot { get; }
  }
}
