﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate {
  public enum ShellColor {
    Black = 0,
    Navy = 1,
    Green = 2,
    Teal = 3,
    Red = 4,
    Purple = 5,
    Gold = 6,
    Silver = 7,
    Gray = 8,
    Blue = 9,
    Lime = 10,
    Aqua = 11,
    Rose = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
    Reset = 16,
  }

  public interface IShell {
    void Write(string value);
    void Write(string format, params object[] arg);

    void WriteLine(string value);
    void WriteLine(string format, params object[] arg);

    ShellColor Color { get; set; }
  }
}
