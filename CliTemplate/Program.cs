﻿using CliTemplate.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate
{
  class Program {

    static int Main(string[] args) {
      CoreApp cli = new CoreApp();
      return (int)cli.Run(args);
    }
  }
}
