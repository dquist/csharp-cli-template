﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTemplate.Util {
  internal static class TextUtil {

    /// <summary>
    /// Expands a list of strings
    /// </summary>
    /// <param name="list">The list of strings</param>
    /// <param name="glue">The glue between each string item</param>
    /// <returns>The imploded text</returns>
    public static string Implode(IList<string> list, string glue) {
      StringBuilder ret = new StringBuilder();
      for (int i = 0; i < list.Count; i++) {
        if (i != 0) {
          ret.Append(glue);
        }
        ret.Append(list[i]);
      }
      return ret.ToString();
    }

  }
}
